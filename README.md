# gametospeech-thesims4

> This mod in now in closed beta. If you want to test it, send an email to [&#x63;&#x6f;&#x6e;&#x74;&#x61;&#x63;&#x74;&#x40;&#x67;&#x61;&#x6d;&#x65;&#x74;&#x6f;&#x73;&#x70;&#x65;&#x65;&#x63;&#x68;&#x2e;&#x63;&#x6f;&#x6d;](mailto:&#x63;&#x6f;&#x6e;&#x74;&#x61;&#x63;&#x74;&#x40;&#x67;&#x61;&#x6d;&#x65;&#x74;&#x6f;&#x73;&#x70;&#x65;&#x65;&#x63;&#x68;&#x2e;&#x63;&#x6f;&#x6d;)!

The GameToSpeech mod for The Sims 4 is a mod that aims to add audio description to the game. For now it is focusing on audio description for live streaming, but later updates will let players have audio description while playing.

**[Learn more about this mod on the GameToSpeech website](https://gametospeech.com/usecase/thesims4)**.

## Installation guide (EN)

You need to follow these steps only at the first installation/launch. The mod will be automatically connected for your next games. If you are a streamer, we advice you to do the installation before starting your stream.

### Installing the mod

1. Download the [Sims4CommunityLibrary](https://github.com/ColonolNutty/Sims4CommunityLibrary/releases/download/v1.75/sims4communitylib.v1.75.zip).
2. Unzip the downloaded file and copy the folder to your Sims 4 Mods folder (located under `Documents/Electronic Arts/The Sims 4/Mods`).
3. Download the [GameToSpeech Sims 4 mod](https://framagit.org/DavidLibeau/gametospeech-thesims4/-/raw/main/Release/gametospeech/gametospeech_mod.ts4script).
4. Copy `gametospeech_mod.ts4script` to your Sims 4 Mods folder.
5. Launch the game and don't forget to activate mods in the Sims 4 options (located under `Game Options > Other`, tick `Enable Custom Content and Mods` and `Scripts Mods Allowed`).
6. Launch a household and connect to the GameToSpeech API with the next instructions.

If you have difficulties installing a mod, you can check this [official guide](https://help.ea.com/en/help/the-sims/the-sims-4/mods-and-the-sims-4-game-updates/).

### Connecting the mod to the GameToSpeech API

1. Login to your [GameToSpeech account](https://api.gametospeech.com/).
2. Generate a new login key: click on `Create login key` on your [account page](https://api.gametospeech.com/profile).
3. Copy this temporary login key.
4. Go back to The Sims 4, in a household, open the command line interface (with `Ctrl + Maj + C`) and write `gametospeech.connect YOUR_LOGIN_KEY` with the login key you copied just before instead of `YOUR_LOGIN_KEY`.
5. You should be connected! You will have a confirmation notification in the game, or you can check that your are connected if you go back to your [GameToSpeech account page](https://api.gametospeech.com/profile) (refresh the page). 

## Guide d'installation (FR)

Vous devez suivre ces étapes qu'une seule fois à la première installation et lancement. Le mod va être automatiquement connecté pour vos prochaines parties. Si vous êtes streameur·euse, il est conseillé de faire cette installation avant de lancer un stream.

### Installer le mod

1. Téléchargez cette librairie: [Sims4CommunityLibrary](https://github.com/ColonolNutty/Sims4CommunityLibrary/releases/download/v1.75/sims4communitylib.v1.75.zip).
2. Dézippez le fichier téléchargé et copiez le dossier dans votre dossier de Mods des Sims 4 (situé ici: `Documents/Electronic Arts/Les Sims 4/Mods`).
3. Téléchargez le [mod GameToSpeech pour Les Sims 4](https://framagit.org/DavidLibeau/gametospeech-thesims4/-/raw/main/Release/gametospeech/gametospeech_mod.ts4script).
4. Copiez `gametospeech_mod.ts4script` dans votre dossier de Mods des Sims 4.
5. Lancez le jeu et n'oubliez pas d'activer les mods dans les options du jeu (situées ici: `Options > Autres`, cochez `Enable Custom Content and Mods` et `Scripts Mods Allowed`).
6. Lancez une partie et connectez vous à l'API GameToSpeech en suivant les instructions ci-après.

Si vous n'arrivez pas à installer de mod, ce [guide officiel](https://help.ea.com/fr/help/the-sims/the-sims-4/mods-and-the-sims-4-game-updates/) peut vous aider.

### Connecter le mod à l'API GameToSpeech

1. Connectez-vous sur votre [compte GameToSpeech](https://api.gametospeech.com/).
2. Générez une clé de connexion: cliquez sur `Create login key` sur la [page de votre compte](https://api.gametospeech.com/profile).
3. Copiez cette clé temporaire.
4. Retrournez dans Les Sims 4, dans une maison, ouvrez l'interface en ligne de commande (avec `Ctrl + Maj + C`) et ecrivez `gametospeech.connect VOTRE_CLÉ_DE_CONNEXION` avec la clé de connexion que vous venez de copier à la place de `VOTRE_CLÉ_DE_CONNEXION`.
5. Vous devrier être connecté·e! Vous allez voir une notification de confirmation dans le jeu, ou vous pouvez vérifier la connexion en revenant sur la [page de compte GameToSpeech](https://api.gametospeech.com/profile) (rafraichissez la page).

