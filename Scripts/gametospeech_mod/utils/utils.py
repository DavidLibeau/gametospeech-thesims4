"""
DavidLibeau
gametospeech.com
"""
import http.client
from sims4communitylib.logging.has_class_log import HasClassLog
from sims4communitylib.notifications.common_basic_notification import CommonBasicNotification
from sims4communitylib.utils.common_json_io_utils import CommonJSONIOUtils
from sims4communitylib.utils.common_io_utils import CommonIOUtils

gametospeech_status = False
gametospeech_dnt = False
gametospeech_server = "api.gametospeech.com"
gametospeech_access_token = ""
gametospeech_user_id = ""


class CommonGametospeechUtils(HasClassLog):
    """Utilities for gametospeech."""

    @classmethod
    def http_get(cls, path: str) -> object:
        """http_get(path)

        Perform a HTTP get request.

        :param params:
        :param path:
        :type params: string
        :type path: string
        :return: res object.
        :rtype: object
        """
        headers = {
            "user-agent": "The Sims 4 (gametospeech mod) gametospeech.com"
        }
        server = cls.get_server().split(":")
        url = server[0]
        port = ""
        if len(server) > 1:
            port = server[1]

        try:
            conn = http.client.HTTPConnection(url, port)
            conn.request("GET", path, "", headers)
            res = conn.getresponse()
            res.body = str(res.read(), "UTF-8")
            conn.close()
            return res
        except OSError:
            cls.set_status(False)
            notification = CommonBasicNotification(
                "Error (GameToSpeech)",
                "Could not reach GameToSpeech server. The mod has been shutted down. Use the command 'gametospeech.start' to restart."
            )
            notification.show()
            return

    @classmethod
    def http_post(cls, path: str, data: str) -> object:
        """http_post(path, data)

        Perform a HTTP get request.

        :param data:
        :param path:
        :type path: string
        :return: res object.
        :rtype: object
        """
        headers = {
            "user-agent": "The Sims 4 (gametospeech mod) gametospeech.com",
            "Accept": "application/json",
            "Content-type": "application/json"
        }
        server = cls.get_server().split(":")
        url = server[0]
        port = ""
        if len(server) > 1:
            port = server[1]

        try:
            conn = http.client.HTTPConnection(url, port)
            conn.request("POST", path, str(data), headers)
            res = conn.getresponse()
            res.body = str(res.read(), "UTF-8")
            conn.close()
            return res
        except OSError:
            cls.set_status(False)
            notification = CommonBasicNotification(
                "Error (GameToSpeech)",
                "Could not reach GameToSpeech server. The mod has been shutted down. Use the command 'gametospeech.start' to restart."
            )
            notification.show()
            return


    @classmethod
    def send_to_api(cls, data: str) -> object:
        """sendToApi(data)

        Send data to web API.

        :param data:
        :type data: str
        :return: result.
        :rtype: bool
        """

        if cls.get_status() is False:
            return

        path = "/thesims4-mod/" + cls.get_user_id() + "?access_token=" + cls.get_access_token()
        res = CommonGametospeechUtils.http_post(path, data)
        if hasattr(res, 'status'):
            if res.status == 200:
                return res
            else:
                notification = CommonBasicNotification(
                    "Error (GameToSpeech): send_to_api",
                    str(res.status) + " " + res.reason
                )
                notification.show()
        return res

    @classmethod
    def startup(cls):
        cls.load_save()
        cls.set_status(True)
        json_data = '{"ping":"test"}'
        res = cls.send_to_api(json_data)
        if hasattr(res, 'status'):
            if res.status == 200:
                notification = CommonBasicNotification(
                    "GameToSpeech started",
                    "API is online!"
                )
                CommonGametospeechUtils.set_status(True)
            else:
                notification = CommonBasicNotification(
                    "GameToSpeech did not started",
                    str(res.status) + " " + res.reason
                )
        else:
            notification = CommonBasicNotification(
                "GameToSpeech did not started",
                "Unknown error. No response from " + cls.get_server()
            )
        notification.show()
        return

    @classmethod
    def get_status(cls):
        global gametospeech_status
        return gametospeech_status

    @classmethod
    def set_status(cls, status: bool):
        global gametospeech_status
        gametospeech_status = status
        return gametospeech_status

    @classmethod
    def get_dnt(cls):
        global gametospeech_dnt
        return gametospeech_dnt

    @classmethod
    def get_dnt_str(cls):
        global gametospeech_dnt
        if gametospeech_dnt is True:
            return "1"
        else:
            return "0"

    @classmethod
    def set_dnt(cls, dnt: bool):
        global gametospeech_dnt
        gametospeech_dnt = dnt
        return gametospeech_dnt

    @classmethod
    def get_server(cls):
        global gametospeech_server
        return gametospeech_server

    @classmethod
    def set_server(cls, server: str):
        global gametospeech_server
        gametospeech_server = server
        cls.save()
        return gametospeech_server

    @classmethod
    def get_access_token(cls):
        if 'gametospeech_access_token' in globals():
            global gametospeech_access_token
            return gametospeech_access_token
        else:
            return ""

    @classmethod
    def set_access_token(cls, access_token: str):
        global gametospeech_access_token
        gametospeech_access_token = access_token
        cls.save()
        return gametospeech_access_token

    @classmethod
    def get_user_id(cls):
        if 'gametospeech_user_id' in globals():
            global gametospeech_user_id
            return gametospeech_user_id
        else:
            return ""

    @classmethod
    def set_user_id(cls, user_id: str):
        global gametospeech_user_id
        gametospeech_user_id = user_id
        cls.save()
        return gametospeech_user_id

    @classmethod
    def save(cls):
        CommonIOUtils.delete_file('gametospeech_settings.json')
        CommonIOUtils.write_to_file('gametospeech_settings.json',
                                    '{"user_id":"' + cls.get_user_id() + '","access_token":"' + cls.get_access_token() + '","server":"' + cls.get_server() + '","dnt":"' + str(cls.get_dnt()) + '"}')
        return

    @classmethod
    def load_save(cls):
        data = CommonJSONIOUtils.load_from_file('gametospeech_settings.json')
        if data:
            cls.set_user_id(data["user_id"])
            cls.set_access_token(data["access_token"])
            cls.set_server(data["server"])
            cls.set_dnt(data["dnt"])
        return
