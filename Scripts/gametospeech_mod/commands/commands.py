"""
DavidLibeau
gametospeech.com
"""
import json

import sims4.commands

from gametospeech_mod.utils.utils import CommonGametospeechUtils
from sims4communitylib.notifications.common_basic_notification import CommonBasicNotification


@sims4.commands.Command('gametospeech.connect', command_type=sims4.commands.CommandType.Live)
def _common_testing_show_input_text_dialog(key: str = "No key provided", _connection: int = None):
    output = sims4.commands.CheatOutput(_connection)
    output('GameToSpeech is connecting… (You should see a notification soon)')

    res = CommonGametospeechUtils.http_get("/connect?login_key=" + key + "&game=thesims4-mod")
    body = json.loads(res.body)
    if res.status == 200:
        CommonGametospeechUtils.set_user_id(body["user_id"])
        CommonGametospeechUtils.set_access_token(body["access_token"])
        notification = CommonBasicNotification(
            "Connected to GameToSpeech",
            "Listen here: https://" + CommonGametospeechUtils.get_server() + "/listen/" + CommonGametospeechUtils.get_user_id()
        )
    else:
        notification = CommonBasicNotification(
            "Error while connecting to GameToSpeech",
            body["error"]
        )
    notification.show()
    return


@sims4.commands.Command('gametospeech.set_server', command_type=sims4.commands.CommandType.Live)
def _common_testing_show_input_text_dialog(server: str = "api.gametospeech.com", _connection: int = None):
    output = sims4.commands.CheatOutput(_connection)
    output('GameToSpeech server should be: ' + server)
    CommonGametospeechUtils.set_server(server)
    return


@sims4.commands.Command('gametospeech.stop', command_type=sims4.commands.CommandType.Live)
def _common_testing_show_input_text_dialog(_connection: int = None):
    output = sims4.commands.CheatOutput(_connection)
    CommonGametospeechUtils.set_status(False)
    output('GameToSpeech mod is now stopped!')
    return


@sims4.commands.Command('gametospeech.start', command_type=sims4.commands.CommandType.Live)
def _common_testing_show_input_text_dialog(_connection: int = None):
    output = sims4.commands.CheatOutput(_connection)
    output('GameToSpeech mod is starting…')
    res = CommonGametospeechUtils.startup()
    return


@sims4.commands.Command('gametospeech.set_dnt', command_type=sims4.commands.CommandType.Live)
def _common_testing_show_input_text_dialog(dnt: bool = False, _connection: int = None):
    output = sims4.commands.CheatOutput(_connection)
    output('GameToSpeech DoNotTrack should be: ' + str(dnt))
    CommonGametospeechUtils.set_dnt(dnt)
    return


@sims4.commands.Command('gametospeech.show_settings', command_type=sims4.commands.CommandType.Live)
def _common_testing_show_input_text_dialog(extended: bool = False, _connection: int = None):
    output = sims4.commands.CheatOutput(_connection)
    output('GameToSpeech settings:')
    output('user_id:' + CommonGametospeechUtils.get_user_id() + ', server:' + CommonGametospeechUtils.get_server())
    if extended:
        output('DoNotTrack:' + CommonGametospeechUtils.get_dnt() + ', access_token:' + CommonGametospeechUtils.get_access_token())
    return

@sims4.commands.Command('gametospeech.version', command_type=sims4.commands.CommandType.Live)
def _common_testing_show_input_text_dialog(_connection: int = None):
    output = sims4.commands.CheatOutput(_connection)
    output('GameToSpeech v0.2')
    return

