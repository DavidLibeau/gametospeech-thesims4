"""
DavidLibeau
gametospeech.com
"""
from sims4communitylib.events.event_handling.common_event_registry import CommonEventRegistry
from sims4communitylib.events.interaction.events.interaction_queued import S4CLInteractionQueuedEvent
from sims4communitylib.modinfo import ModInfo
from sims4communitylib.utils.sims.common_sim_type_utils import CommonSimTypeUtils
from gametospeech_mod.utils.utils import CommonGametospeechUtils


class InteractionEventListener:
    @staticmethod
    @CommonEventRegistry.handle_events(ModInfo.get_identity())
    def handle_event(event_data: S4CLInteractionQueuedEvent) -> bool:
        if not str(event_data.interaction).startswith("Interaction"):
            npc = not CommonSimTypeUtils.is_player_sim(event_data.interaction.sim)
            json_data = '{"type":"interaction", "interaction":"' + str(event_data.interaction) + '","action":"' + event_data.interaction.__class__.__name__ + '","target":"' + str(event_data.interaction.target) + '","character":"' + str(event_data.interaction.sim) + '","npc":"' + str(npc) + '", "dnt":"'+CommonGametospeechUtils.get_dnt_str()+'"}'
            CommonGametospeechUtils.send_to_api(json_data)
        return True
