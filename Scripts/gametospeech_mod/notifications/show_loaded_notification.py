"""
DavidLibeau
gametospeech.com
"""
from sims4communitylib.events.event_handling.common_event_registry import CommonEventRegistry
from sims4communitylib.events.zone_spin.events.zone_late_load import S4CLZoneLateLoadEvent
from gametospeech_mod.utils.utils import CommonGametospeechUtils


class GameToSpeechModShowLoadedMessage:
    @staticmethod
    @CommonEventRegistry.handle_events('gametospeech_mod')
    def _show_loaded_notification_when_loaded(event_data: S4CLZoneLateLoadEvent):
        if event_data.game_loaded:
            # If the game has not loaded yet, we don't want to show our notification.
            return
        CommonGametospeechUtils.startup()